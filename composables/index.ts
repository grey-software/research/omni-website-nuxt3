import { useMediaQuery } from "@vueuse/core";

export const useScreenMobSm = useMediaQuery("max-width: 399px");
export const useScreenMobLg = useMediaQuery(
  "(min-width: 400px) and (max-width: 599px)"
);
export const useScreenTabPt = useMediaQuery(
  "(min-width: 600px) and (max-width: 899px)"
);
export const useScreenTabLs = useMediaQuery(
  "(min-width: 900px) and (max-width: 1199px)"
);
export const useScreenPcSm = useMediaQuery(
  "(min-width: 1200px) and (max-width: 1799px)"
);
export const useScreenPcLg = useMediaQuery("(min-width: 1800px)");
